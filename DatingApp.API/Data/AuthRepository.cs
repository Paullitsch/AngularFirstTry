using System;
using System.Threading.Tasks;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;

namespace DatingApp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        public AuthRepository(DataContext context)
        {
            this._context = context;
        }
        public async Task<User> Login(string username, string password)
        {
            //Was passiert eigentlich wenn hier ne Exception kommt?
            var user = await _context.Users.SingleOrDefaultAsync(x => x.Username == username);

            if (user == null)
                return null;

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var result = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < result.Length; i++)
                {
                    if (result[i] != passwordHash[i])
                        return false;
                }
            }
            return true;
        }

        public async Task<User> Register(string username, string password)
        {
            var result = CreatePasswordHash(password);

            var user = new User()
            {
                Username = username,
                PasswordHash = result.PasswordHash,
                PasswordSalt = result.PasswordSalt
            };

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private (byte[] PasswordSalt, byte[] PasswordHash) CreatePasswordHash(string password)
        {
            (byte[] PasswordSalt, byte[] PasswordHash) result;

            using (var hmac = new HMACSHA512())
                result = (PasswordSalt: hmac.Key, PasswordHash: hmac.ComputeHash(Encoding.UTF8.GetBytes(password)));

            return result;
        }

        public async Task<bool> UserExists(string username)
        {
            return await _context.Users.AnyAsync(x => x.Username == username);
        }
    }
}