using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DatingApp.API.Models;
using Newtonsoft.Json;

namespace DatingApp.API.Data
{
    public class Seed
    {
        private readonly DataContext _context;
        public Seed(DataContext context)
        {
            _context = context;
        }

        public void SeedUsers()
        {
            var userData = File.ReadAllText("Data/UserSeedDataGuide.json");
            var users = JsonConvert.DeserializeObject<List<User>>(userData);
            foreach (var user in users)
            {
                var result = CreatePasswordHash("password");
                user.PasswordHash = result.PasswordHash;
                user.PasswordSalt = result.PasswordSalt;
                user.Username = user.Username.ToLower();
                _context.Users.Add(user);
            }

            _context.SaveChangesAsync();
        }

        private (byte[] PasswordSalt, byte[] PasswordHash) CreatePasswordHash(string password)
        {
            (byte[] PasswordSalt, byte[] PasswordHash) result;

            using (var hmac = new HMACSHA512())
                result = (PasswordSalt: hmac.Key, PasswordHash: hmac.ComputeHash(Encoding.UTF8.GetBytes(password)));

            return result;
        }
    }
}